from haystack import Finder
from haystack.preprocessor.cleaning import clean_wiki_text
from haystack.preprocessor.utils import convert_files_to_dicts
from haystack.reader.farm import FARMReader
from haystack.reader.transformers import TransformersReader
from haystack.utils import print_answers
from haystack.document_store.memory import InMemoryDocumentStore
from haystack.file_converter.pdf import PDFToTextConverter
from haystack.retriever.sparse import TfidfRetriever

document_store = InMemoryDocumentStore()

file = './pdf-test.pdf'
converter = PDFToTextConverter(remove_numeric_tables=True, valid_languages=["de"])
doc = converter.convert(file_path=file, meta=None)

document_store.write_documents([doc])

print(document_store.get_all_documents())

retriever = TfidfRetriever(document_store=document_store)

reader = FARMReader(model_name_or_path="salti/bert-base-multilingual-cased-finetuned-squad", use_gpu=False)
finder = Finder(reader, retriever)

prediction = finder.get_answers(question="wie ist die bundeswehr eingebunden?", top_k_retriever=10, top_k_reader=5)
print_answers(prediction, details="minimal")